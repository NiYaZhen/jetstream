import { ControllerMetadata } from './jetstream.interface';
export class ControllerService {
  async getAllControllers(path: string) {
    const controllers = await import(path);
    return Object.values(controllers).map((Class: any) => new Class());
  }

  getControllerMetadata(controller: any): ControllerMetadata {
    const ControllerClass = controller.constructor;
    return {
      consumer: Reflect.getMetadata('consumer', ControllerClass),
      subscribers: Reflect.getMetadata('subscribers', controller) || [],
      repliers: Reflect.getMetadata('repliers', controller) || []
    };
  }
}
