import {
  Codec,
  JSONCodec,
  JetStreamClient,
  JetStreamManager,
  JsMsg,
  Msg,
  NatsConnection,
  connect
} from 'nats';
import {
  NatsServerConfig,
  NatsStreamConfig,
  TransferInfo
} from './jetstream.interface';

export class JetStreamService {
  replyMessage!: TransferInfo<any>;
  #streamConfig!: NatsStreamConfig;
  #natsConnection!: NatsConnection;
  #jetStreamManager!: JetStreamManager;
  #jetStreamClient!: JetStreamClient;
  #jsonCodec: Codec<any> = JSONCodec();
  #url: string = '';

  constructor(private readonly serverConfig: NatsServerConfig) {}

  async connect() {
    if (this.#natsConnection) return;

    this.#natsConnection = await connect(this.serverConfig);
    this.#jetStreamManager = await this.#natsConnection.jetstreamManager();
    this.#jetStreamClient = await this.#natsConnection.jetstream();
  }

  async publish<T>(subject: string, info: TransferInfo<T>) {
    try {
      await this.#jetStreamClient.publish(
        subject,
        this.#jsonCodec.encode(info)
      );
    } catch (error) {
      console.error(error);
    }
  }

  async subscribe(
    name: string,
    callback: (message: JsMsg, payload: any) => void
  ) {
    const consumers = await this.#jetStreamClient.consumers.get(
      this.serverConfig.stream,
      name
    );

    const messages = await consumers.consume();
    for await (const message of messages) {
      try {
        const payload = this.#jsonCodec.decode(message.data);
        console.log(`Got message from subject ${message.subject}: ${payload}`);
        callback(message, payload);
      } catch (error) {
        console.error(
          `Error getting message of subject ${message.subject}:`,
          error
        );
      }
    }
  }

  reply(
    subject: string,
    callback: (message: Msg, payload: any, jsonCodec: Codec<any>) => void
  ) {
    this.#natsConnection.subscribe(subject, {
      callback: (_err, message) => {
        const payload = this.#jsonCodec.decode(message.data);
        console.log(`Got request from subject ${message.subject}: ${payload}`);

        callback(message, payload, this.#jsonCodec);
      }
    });
  }

  // 取消連線
  async drain() {
    await this.#natsConnection.drain();
  }
}
