"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliverPolicy = exports.RetentionPolicy = exports.AckPolicy = exports.SubscribeType = void 0;
var SubscribeType;
(function (SubscribeType) {
    SubscribeType["Pull"] = "pull";
    SubscribeType["Push"] = "push";
})(SubscribeType || (exports.SubscribeType = SubscribeType = {}));
var AckPolicy;
(function (AckPolicy) {
    /**
     * 不確認任何消息
     */
    AckPolicy["None"] = "none";
    /**
     * 確認收到的最後一個消息，之前收到的所有消息將在同一時間自動確認
     */
    AckPolicy["All"] = "all";
    /**
     * 每個個別消息都必須被確認
     */
    AckPolicy["Explicit"] = "explicit";
    /**
     * @ignore
     */
    AckPolicy["NotSet"] = "";
})(AckPolicy || (exports.AckPolicy = AckPolicy = {}));
var RetentionPolicy;
(function (RetentionPolicy) {
    /**
     * 保留訊息直到達到限制，然後觸發丟棄策略。
     */
    RetentionPolicy["Limits"] = "limits";
    /**
     * 在特定主題上存在消費者興趣時保留訊息。
     */
    RetentionPolicy["Interest"] = "interest";
    /**
     * 保留訊息直到被確認。
     */
    RetentionPolicy["Workqueue"] = "workqueue";
})(RetentionPolicy || (exports.RetentionPolicy = RetentionPolicy = {}));
var DeliverPolicy;
(function (DeliverPolicy) {
    /**
     * Deliver all messages
     */
    DeliverPolicy["All"] = "all";
    /**
     * Deliver starting with the last message
     */
    DeliverPolicy["Last"] = "last";
    /**
     * Deliver starting with new messages
     */
    DeliverPolicy["New"] = "new";
    /**
     * Deliver starting with the specified sequence
     */
    DeliverPolicy["StartSequence"] = "by_start_sequence";
    /**
     * Deliver starting with the specified time
     */
    DeliverPolicy["StartTime"] = "by_start_time";
    /**
     * Deliver starting with the last messages for every subject
     */
    DeliverPolicy["LastPerSubject"] = "last_per_subject";
})(DeliverPolicy || (exports.DeliverPolicy = DeliverPolicy = {}));
