import { ControllerMetadata } from './jetstream.interface';
export declare class ControllerService {
    getAllControllers(path: string): Promise<any[]>;
    getControllerMetadata(controller: any): ControllerMetadata;
}
