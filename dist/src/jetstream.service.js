"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JetStreamService = void 0;
const nats_1 = require("nats");
class JetStreamService {
    serverConfig;
    replyMessage;
    #streamConfig;
    #natsConnection;
    #jetStreamManager;
    #jetStreamClient;
    #jsonCodec = (0, nats_1.JSONCodec)();
    #url = '';
    constructor(serverConfig) {
        this.serverConfig = serverConfig;
    }
    async connect() {
        if (this.#natsConnection)
            return;
        this.#natsConnection = await (0, nats_1.connect)(this.serverConfig);
        this.#jetStreamManager = await this.#natsConnection.jetstreamManager();
        this.#jetStreamClient = await this.#natsConnection.jetstream();
    }
    async publish(subject, info) {
        try {
            await this.#jetStreamClient.publish(subject, this.#jsonCodec.encode(info));
        }
        catch (error) {
            console.error(error);
        }
    }
    async subscribe(name, callback) {
        const consumers = await this.#jetStreamClient.consumers.get(this.serverConfig.stream, name);
        const messages = await consumers.consume();
        for await (const message of messages) {
            try {
                const payload = this.#jsonCodec.decode(message.data);
                console.log(`Got message from subject ${message.subject}: ${payload}`);
                callback(message, payload);
            }
            catch (error) {
                console.error(`Error getting message of subject ${message.subject}:`, error);
            }
        }
    }
    reply(subject, callback) {
        this.#natsConnection.subscribe(subject, {
            callback: (_err, message) => {
                const payload = this.#jsonCodec.decode(message.data);
                console.log(`Got request from subject ${message.subject}: ${payload}`);
                callback(message, payload, this.#jsonCodec);
            }
        });
    }
    // 取消連線
    async drain() {
        await this.#natsConnection.drain();
    }
}
exports.JetStreamService = JetStreamService;
