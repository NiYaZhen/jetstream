"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Replier = exports.Subscriber = exports.Controller = void 0;
require("reflect-metadata");
function Controller(consumer) {
    return (constructor) => {
        Reflect.defineMetadata('consumer', consumer, constructor);
    };
}
exports.Controller = Controller;
function Subscriber(subject = '') {
    return (target, propertyKey) => {
        const subscribers = Reflect.getMetadata('subscribers', target) || [];
        subscribers.push({ subject, methodName: propertyKey });
        Reflect.defineMetadata('subscribers', subscribers, target);
    };
}
exports.Subscriber = Subscriber;
function Replier(subject = '') {
    return (target, propertyKey) => {
        const repliers = Reflect.getMetadata('repliers', target) || [];
        repliers.push({ subject, methodName: propertyKey });
        Reflect.defineMetadata('repliers', repliers, target);
    };
}
exports.Replier = Replier;
