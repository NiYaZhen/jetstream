import 'reflect-metadata';
export declare function Controller(consumer: string): (constructor: Function) => void;
export declare function Subscriber(subject?: string): (target: any, propertyKey: string) => void;
export declare function Replier(subject?: string): (target: any, propertyKey: string) => void;
