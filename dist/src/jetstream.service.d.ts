import { Codec, JsMsg, Msg } from 'nats';
import { NatsServerConfig, TransferInfo } from './jetstream.interface';
export declare class JetStreamService {
    #private;
    private readonly serverConfig;
    replyMessage: TransferInfo<any>;
    constructor(serverConfig: NatsServerConfig);
    connect(): Promise<void>;
    publish<T>(subject: string, info: TransferInfo<T>): Promise<void>;
    subscribe(name: string, callback: (message: JsMsg, payload: any) => void): Promise<void>;
    reply(subject: string, callback: (message: Msg, payload: any, jsonCodec: Codec<any>) => void): void;
    drain(): Promise<void>;
}
